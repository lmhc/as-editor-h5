# AS-Editor-H5

#### 介绍

基于 vue 开发的移动端模版，可以通过定义的JSON生成页面。与 [AS-Editor](https://gitee.com/was666/as-editor.git) 一起使用可以可视化生成vue页面

#### 安装教程

1.  npm install （安装 node_modules 模块）
2.  npm run serve （运行）
3.  npm run build （打包）

#### 使用说明

##### 使用教程

> home.vue 页面为demo实例

![步骤1](https://images.gitee.com/uploads/images/2021/1027/180625_a6903982_5546746.png "image1.png")
![步骤2](https://images.gitee.com/uploads/images/2021/1027/180644_1710b3f0_5546746.png "image2.png")
![步骤3](https://images.gitee.com/uploads/images/2021/1027/180656_e71a6d53_5546746.png "image3.png")


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


