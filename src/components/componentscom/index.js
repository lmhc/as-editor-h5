export { default as captiontext } from './captiontext' // 标题文字
export { default as pictureads } from './pictureads' // 图片广告
export { default as placementarea } from './placementarea' // 组件安置区
export { default as graphicnavigation } from './graphicnavigation' // 图形导航
export { default as richtext } from './richtext' // 富文本
export { default as magiccube } from './magiccube' // 魔方
export { default as auxiliarysegmentation } from './auxiliarysegmentation' // 辅助分割
export { default as commoditysearch } from './commoditysearch' // 商品搜索
export { default as storeinformation } from './storeinformation' // 店铺信息
export { default as entertheshop } from './entertheshop' //    进入店铺
export { default as notice } from './notice' // 公告
export { default as videoss } from './videoss' // 视频
export { default as voicer } from './voicer' // 音频
export { default as custommodule } from './custommodule'
export { default as communitypowder } from './communitypowder' //  社区涨粉
export { default as storenotecard } from './storenotecard' // 店铺笔记卡
export { default as crowdoperation } from './crowdoperation'
export { default as personalizedrecommendation } from './personalizedrecommendation'
export { default as onlineservice } from './onlineservice'
export { default as listswitching } from './listswitching' // 商品
export { default as investigate } from './investigate' // 商品
export { default as tabBar } from './tabBar'
export { default as follow } from './follow'
export { default as suspension } from './suspension'
